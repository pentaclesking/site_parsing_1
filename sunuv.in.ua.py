from typing import TypedDict
import re
import time
from bs4 import BeautifulSoup
import constants
import requests
import openpyxl
from pathlib import Path
import asyncio
import aiohttp
from pprint import pprint
import platform


class Variant(TypedDict):
    name: str
    in_stock: bool


class Product(TypedDict):
    name: str
    art: str | None
    price: int
    old_price: int | None
    available: str
    link: str
    variant: str | None


headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:77.0) Gecko/20100101 Firefox/77.0'}
price_file = Path(constants.supl_path) / 'sunuv.in.ua.xlsx'
art_clmn = 1
name_clmn = 2
available_clmn = 3
price_clmn = 4
oldprice_clmn = 5
group_clmn = 6
link_clmn = 7
variant_clmn = 8

site = 'https://sunuv.in.ua/'


def get_page(link: str) -> str:
    return requests.get(link, headers=headers).text


def get_soup(link: str) -> BeautifulSoup:
    return BeautifulSoup(get_page(link), 'html.parser')


def get_categories_links(link) -> list:
    soup = get_soup(link)
    links = [a['href'] for a in soup.find(id="menu-item-393").ul.find_all('a')]
    return links


def get_products_links(category_link) -> list:
    soup = get_soup(category_link)
    products_links = [li.a['href'] for li in soup.select_one(".products.columns-3").find_all('li')]
    return products_links


def get_number(price) -> int:
    return int(round((int(''.join(re.findall(r'\d', price))) - 100) * 1.25, 0))


def find_tag_by_text(tags, text):
    for tag in tags:
        if text in tag.text:
            return tag
    return None


def get_variants(soup: BeautifulSoup) -> list[Variant]:
    if soup.find('td', {'class': 'value'}):
        variants_tags = soup.find('td', {'class': 'value'}).ul.find_all('li')
        return [Variant(name=variant.text, in_stock=False if 'disabled' in variant['class'] else True) for variant in variants_tags]


def get_name(soup):
    return soup.find('h1').text


def get_art(soup):
    if art := soup.find('span', {'class': 'sku'}):
        art = art.text
        return art
    else:
        return None


def get_available(soup):
    if available := soup.find('button', {'class': 'single_add_to_cart_button'}):
        available = '+' if available.text.lower() == 'купить' else '-'
    if soup.find('p', {'class': 'out-of-stock'}):
        available = '-'
    return available


def get_prices(soup):
    price_tag = soup.find('p', {'class': 'price'})
    prices = [get_number(i) for i in price_tag.text.split('–')]    # нестандартный длинный дефис
    return prices


async def get_product_info(product_link, session) -> list[Product] | None:
    old_price = None
    # soup = get_soup(product_link)
    soup = await get_soup_async(product_link, session)

    name = get_name(soup)
    art = get_art(soup)
    available = get_available(soup)
    prices = get_prices(soup)

    if len(prices) not in (1, 2):
        constants.send_message(f"Ошибка в программе {__file__}\n В товаре {product_link} {len(prices)} цен")
        return None

    variants = get_variants(soup)
    if variants:
        if len(prices) == 2 and len(variants) != 2:
            constants.send_message(f"Ошибка в программе {__file__}\n В товаре {product_link} найдено более 2-х "
                                   f"вариантов. Невозможно сопоставить цену и название")
            return None
        return [Product(name=f'{name} - {variant["name"]}',
                        art=art,
                        price=prices[index] if len(prices) == 2 else prices[0],
                        old_price=old_price,
                        available='+' if variant['in_stock'] else '-',
                        link=product_link,
                        variant=variant['name'])
                for index, variant in enumerate(variants)]
    else:
        return [Product(name=f'{name}', art=art, price=prices[0], old_price=old_price,
                        available=available, link=product_link, variant=None)]


def get_links_for_processing(site):
    check_duplicates = set()     # only uniqu
    for category_link in get_categories_links(site):
        time.sleep(0.5)
        for product_link in get_products_links(category_link):
            check_duplicates.add(product_link)
    return check_duplicates


def write_product_to_xls(product: Product, sheet, row):
    sheet.cell(row, art_clmn).value = product['art']
    sheet.cell(row, name_clmn).value = product['name']
    sheet.cell(row, price_clmn).value = product['old_price'] if product['old_price'] else product['price']
    sheet.cell(row, available_clmn).value = product['available']
    sheet.cell(row, link_clmn).value = product['link']
    sheet.cell(row, variant_clmn).value = product['variant']


async def get_page_async(url: str, session):
    r = await session.request(method='GET', url=url)
    text = await r.text()
    return text


async def get_soup_async(url, session) -> BeautifulSoup:
    text = await get_page_async(url, session)
    return BeautifulSoup(text, 'html.parser')


async def worker(i: int, session):
    products = []
    while True:
        try:
            product_link = site_links.pop()
        except KeyError:
            return products
        else:
            print(f'\nWorker {i} ---- Getting {product_link}')
            try:
                products_info = await get_product_info(product_link, session)
                print('\n', products_info, sep='')
                if products_info is None:
                    continue
            except Exception as e:
                print(f'\n\n\n{str(e)}\n\n\n')
                constants.send_message(f"Ошибка в программе {__file__}\n{str(e)}\n{product_link}")
            else:
                for product in products_info:
                    products.append(product)
                    # print(f'Worker {i} ---- Writing {product}')
                    # write_product_to_xls(product=product)
                    # current_row += 1
            await asyncio.sleep(.5)


async def main(workers):
    async with aiohttp.ClientSession() as session:
        result = await asyncio.gather(*[worker(i, session) for i in range(workers)])
    return result


t0 = time.time()
wb = openpyxl.Workbook()
sheet = wb.active
current_row = 2

site_links = get_links_for_processing(site)

if platform.system() == 'Windows':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

all_products_list = []
for product_list in asyncio.run(main(constants.number_of_workers)):
    all_products_list.extend(product_list)

for product in all_products_list:
    write_product_to_xls(product, sheet, current_row)
    current_row += 1

wb.save(price_file)
t1 = time.time()
print(f'\nTime = {t1-t0:.02f}')
